﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBlogVer2
{
    public partial class Search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["PostId"] == null)
                TextBox1.Text = "1";
            else
                TextBox1.Text = Request.QueryString["PostId"];
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string subs = TextBox1.Text;
            Response.Redirect("~/Post.aspx?PostId=" + subs);
        }
    }
}