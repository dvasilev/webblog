﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="WebBlogVer2.Post" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="generalPage" runat="server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BlogDBConnectionString %>" SelectCommand="Select [Content], [Header], [PublicationDate] From [Posts] Where ([Id] = @DbVar) ">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="1" Name="DbVar" QueryStringField="PostId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:FormView DefaultMode="ReadOnly" ID="DetailsView1" CssClass="dataListMiddle" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <div class="mainPageBody">
                <h2>
                    <%# Eval("Header") %>
                </h2>
                <div class="publicationDateMainPage">
                    <%# ((DateTime)Eval("PublicationDate")).ToShortDateString() %>
                </div>
            </div>

            <div class="contentPostPage">
                <%# Eval("Content") %>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <h3>Пост с этим номером не существует!
            </h3>
        </EmptyDataTemplate>
    </asp:FormView>
    <pre id="dictionary" runat="server">
    </pre>
    <asp:Label ID="Label1" runat="server" Height="200px" Width="400px"></asp:Label>

</asp:Content>
