﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="EditPage.aspx.cs" Inherits="WebBlogVer2.EditPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="GeneralPage" runat="server">
    <asp:FormView ID="FormView1" CssClass="mainDivEditPage" runat="server" DataKeyNames="Id" DataSourceID="SqlDataSource1" DefaultMode="Edit">
        <EditItemTemplate>
            <div>
                <span class="textEditYourPostEditPostPage">Edit your post </span>
                <hr />
                <div class="editContanier">
                    <asp:Label ID="Label" Text="Header" runat="server" CssClass="labelEditPage inner"></asp:Label>
                    <asp:TextBox Text='<%# Bind("Header") %>' TextMode="MultiLine" Rows="1" Columns="55" CssClass="textBoxEditPage " ID="TextBox1" runat="server"></asp:TextBox>
                </div>
            </div>
            <div>
                <asp:Label Text="Description" runat="server" CssClass="labelEditPage inner"></asp:Label>
                <asp:TextBox Text='<%# Bind("Description") %>' TextMode="MultiLine" Rows="3" Columns="55" ID="TextBox2" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label Text="Content" runat="server" CssClass="labelEditPage inner"></asp:Label>
                <asp:TextBox Text='<%# Bind("Content") %>' ID="TextBox3" TextMode="MultiLIne" Rows="5" Columns="55" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label Text="Publication Date" runat="server" CssClass="labelEditPage"></asp:Label>
                <span>
                    <asp:Label CssClass="labelEditPage2" Text='<%# ((DateTime)Eval("PublicationDate")).ToShortDateString() %>' runat="server"></asp:Label></span>
            </div>
            <div class="updateButtonEditPage">
                <asp:Button ID="UpdateButton"
                    Text="Update"
                    CommandName="Update"
                    runat="server" />
            </div>
        </EditItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" OnUpdated="SqlDataSource1_Updated" ConnectionString="<%$ ConnectionStrings:BlogDBConnectionString %>" SelectCommand="SELECT * FROM [Posts] WHERE ([Id] = @DbVar)" UpdateCommand="UPDATE [Posts] SET [Header] = @Header, [Description] = @Description, [Content] = @Content WHERE [Id] = @Id">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="1" Name="DbVar" QueryStringField="PostId" Type="Int32" />
        </SelectParameters>

        <UpdateParameters>
            <asp:Parameter Name="Header" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Content" Type="String" />
            <asp:Parameter Name="Id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

</asp:Content>

