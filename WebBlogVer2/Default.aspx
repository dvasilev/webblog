﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebBlogVer2.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="searchContanier">
        <asp:HyperLink ID="SearchHyperLink" runat="server" Text="Search" NavigateUrl="Search.aspx" CssClass="defaultHyperLink"></asp:HyperLink>
    </div>
    <div class="searchContanier">
        <asp:HyperLink ID="HyperLink4" runat="server" Text="Add" NavigateUrl="Add.aspx" CssClass="defaultHyperLink"></asp:HyperLink>
    </div>
    <div class="mainBlogHead">
        by Vasilev Dmitrii
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="GeneralPage" runat="server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BlogDBConnectionString %>" SelectCommand="SELECT * FROM [Posts] Order By Id desc" DeleteCommand="DELETE FROM [Posts] WHERE [Id] = @Id">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <asp:DataList ID="DataList1" runat="server" OnDeleteCommand="DataList1_DeleteCommand" CssClass="dataListMiddle" DataKeyField="Id" DataSourceID="SqlDataSource1">
        <ItemTemplate>

            <div class="mainPageBody">
                <h2>
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="linkHeaderMainPage" NavigateUrl='<%# "Post.aspx?PostId=" + Eval("Id") %>' Text='<%# Eval("Header") %>'></asp:HyperLink>
                </h2>
                <asp:Label ID="PublicationDateLabel" runat="server" CssClass="publicationDateMainPage" Text='<%# ((DateTime)Eval("PublicationDate")).ToShortDateString()  %>'></asp:Label>
            </div>

            <asp:Label CssClass="contentDefaultPage" ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>'></asp:Label>

            <div class="editHyperLinkDefaultPage">
                <asp:HyperLink ID="HyperLink2" CssClass="defaultHyperLink" runat="server" NavigateUrl='<%# "EditPage.aspx?PostId=" + Eval("Id") %>' Text="Edit"></asp:HyperLink>
                <asp:LinkButton ID="DeleteHyperLink" CssClass="defaultHyperLink" CommandName="Delete" CommandArgument='<%# Eval("Id") %>' runat="server" Text="Delete"></asp:LinkButton>
            </div>
        </ItemTemplate>
    </asp:DataList>
</asp:Content>

