﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Add.aspx.cs" Inherits="WebBlogVer2.Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="GeneralPage" runat="server">
    <asp:FormView ID="FormView1" CssClass="mainDivEditPage" runat="server" DataKeyNames="Id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <InsertItemTemplate>
            <div>
                <span class="textEditYourPostEditPostPage">Add new post </span>
                <hr />
                <div class="editContanier">
                    <asp:Label ID="Label" Text="Header" runat="server" CssClass="labelEditPage inner"></asp:Label>
                    <asp:TextBox Text='<%# Bind("Header") %>' TextMode="MultiLine" Rows="1" Columns="55" CssClass="textBoxEditPage " ID="TextBox1" runat="server"></asp:TextBox>
                </div>
            </div>
            <div>
                <asp:Label Text="Description" runat="server" CssClass="labelEditPage inner"></asp:Label>
                <asp:TextBox Text='<%# Bind("Description") %>' TextMode="MultiLine" Rows="3" Columns="55" ID="TextBox2" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label Text="Content" runat="server" CssClass="labelEditPage inner"></asp:Label>
                <asp:TextBox Text='<%# Bind("Content") %>' ID="TextBox3" TextMode="MultiLIne" Rows="5" Columns="55" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label Text="Publication Date" runat="server" CssClass="labelEditPage"></asp:Label>
                <span>
                    <asp:Label CssClass="labelEditPage2" Text='<%# (DateTime.Now).ToShortDateString() %>' runat="server"></asp:Label></span>
            </div>
            <div class="updateButtonEditPage">
                <asp:Button ID="InsertButton"
                    Text="Add"
                    CommandName="Insert"
                    runat="server" />
            </div>
        </InsertItemTemplate>
    </asp:FormView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" OnInserted="SqlDataSource1_Inserted" ConnectionString="<%$ ConnectionStrings:BlogDBConnectionString %>" InsertCommand="INSERT INTO [Posts] ([Header], [Description], [Content], [PublicationDate]) VALUES (@Header, @Description, @Content, GetDate())">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="1" Name="DbVar" QueryStringField="PostId" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="Header" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Content" Type="String" />
        </InsertParameters>
    </asp:SqlDataSource>
</asp:Content>

